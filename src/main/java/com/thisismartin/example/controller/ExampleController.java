package com.thisismartin.example.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * 
 * @author martin.spasovski
 * 
 */
@Controller
public class ExampleController {

	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String home(Model model) {
		return "pages/main";
	}

	@RequestMapping(value = "/about", method = RequestMethod.GET)
	public String about(Model model) {
		return "pages/about";
	}

	@RequestMapping(value = { "/example1" }, method = RequestMethod.GET)
	public String example1(Model model) {
		return "pages/example1";
	}

	@RequestMapping(value = { "/example2" }, method = RequestMethod.GET)
	public String example2(Model model) {
		return "pages/example2";
	}

	@RequestMapping(value = { "/example3" }, method = RequestMethod.GET)
	public String example3(Model model) {
		return "pages/example3";
	}

	@RequestMapping(value = { "/example4" }, method = RequestMethod.GET)
	public String example4(Model model) {
		return "pages/example4";
	}

}
