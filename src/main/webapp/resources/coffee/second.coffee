namespace = (target, name, block) ->
  [target, name, block] = [(if typeof exports isnt 'undefined' then exports else window), arguments...] if arguments.length < 3
  top = target
  target = target[item] or= {} for item in name.split '.'
  block target, top

namespace "APP", (exports) ->

  exports.draw = ->
    canvas = document.getElementById("canvas")
    ctx = canvas.getContext("2d")
    ctx.beginPath() 
    ctx.arc Math.random(0, 100)*500, Math.random(0, 100)*200, 10, 0, Math.PI*2, true
    ctx.closePath()
    ctx.fillStyle = '#'+Math.floor(Math.random()*16777215).toString(16)
    ctx.fill()
    